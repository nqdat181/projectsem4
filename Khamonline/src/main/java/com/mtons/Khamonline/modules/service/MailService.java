package com.mtons.Khamonline.modules.service;

import java.util.Map;

/**
 * @author : khamonline
 */
public interface MailService {
    void config();
    void sendTemplateEmail(String to, String title, String template, Map<String, Object> content);
}
