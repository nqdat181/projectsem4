package com.mtons.Khamonline.modules.service;

import com.mtons.Khamonline.modules.entity.Links;

import java.util.List;

/**
 * @author : khamonline
 */
public interface LinksService {
    List<Links> findAll();
    void update(Links links);
    void delete(long id);
}
